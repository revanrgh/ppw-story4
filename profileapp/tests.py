from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home

# Create your tests here.
class ProfileWebUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_home_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home.html')
		
	def test_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)