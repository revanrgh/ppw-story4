from django import forms
from .models import Comment
import datetime

class CommentForm(forms.Form):
	comment_attrs = {
        'type': 'text',
        'class' : 'form-control',
        'placeholder':'Please type your comment here...',
        'id':'id_comment'
    }

	comment = forms.CharField(label = '', required = True, max_length = 300, widget=forms.TextInput(attrs = comment_attrs))
