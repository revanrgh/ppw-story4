from django.db import models

# Create your models
class Comment(models.Model):
	comment = models.CharField(max_length = 300)
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.comment