from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import CommentForm
from .models import Comment

response = {}

def home(request):
    comment_all = Comment.objects.all()
    response['comment_all'] = comment_all
    response['comment_form'] = CommentForm
    return render(request, 'home.html', response)

def post(request):
    comment_form = CommentForm(request.POST or None)
    if(request.method == 'POST' and comment_form.is_valid()):
        response['comment'] = request.POST['comment']
        comment = Comment(comment=response['comment'])
        comment.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
